@extends('tampilan.layout')
@section('judul')
Form Login
@endsection

@section('content')
<form method="post" action="/kirim">
	@csrf
	First Name	:	<br/><input type="text" name="firstname" required ><br/>
	Last Name	:	<br/><input type="text" name="lastname" required ><br/>
	Gender		:	<br/>
                    <input type="radio" name="gender" value="male" /> Male<br/>
					<input type="radio" name="gender" value="female"/> Female<br/>
					<input type="radio" name="gender" value="other"/> Other<br/>
					<br/>
	Nationality :	<br/><br/>
                    <select name="nasional">
							<option value="ina">Indonesia</option>
							<option value="usa">Amerika</option>
							<option value="eng">Inggris</option>
					</select>
					<br/>
                    <br/>
	Language Spoken : <br/>
					<input type="checkbox" name="bhi"> Bahasa Indonesia <br/>
					<input type="checkbox" name="eng"> English <br/>
					<input type="checkbox" name="Other"> Other <br/>
					<br/>
	Bio			:	<br/>
                    <textarea name="bio" cols="30" rows="5"></textarea>
					<br/>
					
					<input type="submit" value="kirim">
    </form>
@endsection