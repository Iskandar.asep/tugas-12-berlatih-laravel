@extends('tampilan.layout')
@section('judul')
Form Cast
@endsection

@section('content')

<form action="/cast" method="POST">
@csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="name" class="form-control" placeholder="Ketikan nama">
  </div>
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="umur" class="form-control" placeholder="Ketikan umur">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" class="form-control" cols="30" rows="10" placeholder="Ketikan Bio Anda"> </textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Tambah</button>
</form>


@endsection