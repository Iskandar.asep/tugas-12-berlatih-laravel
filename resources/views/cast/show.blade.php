@extends('tampilan.layout')
@section('judul')
Detail Cast : {{$cast->name}}
@endsection

@section('content')

<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">{{$cast->id}}</th>
      <td>{{$cast->name}}</td>
      <td>{{$cast->umur}}</td>
      <td>{{$cast->bio}}</td>
    </tr>
  </tbody>
</table>
  <a href="/cast" class="btn btn-info btn-sm" >Kembali</a>

@endsection