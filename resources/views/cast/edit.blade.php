@extends('tampilan.layout')
@section('judul')
Form Edit Cast : {{$cast->name}}
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
@csrf
@method('put')
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="name" class="form-control" value="{{$cast->name}}">
  </div>
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" name="umur" class="form-control" value="{{$cast->umur}}">
  </div>
  @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" class="form-control" cols="30" rows="10"> {{$cast->bio}} </textarea>

  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>
  <button type="submit" class="btn btn-primary">Update</button>
</form>


@endsection