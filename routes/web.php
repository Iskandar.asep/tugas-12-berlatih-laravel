<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/form', 'AuthController@form');
Route::post('/kirim', 'AuthController@kirim');
Route::get('/table', 'TbController@table');

Route::get('/data-table', function(){
return view('table.dttable');
});

//CRUD Cast
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@simpan');
Route::get('/cast', 'CastController@tampil');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');